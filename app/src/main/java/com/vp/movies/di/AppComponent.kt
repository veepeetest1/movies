package com.vp.movies.di

import android.content.Context
import com.squareup.inject.assisted.dagger2.AssistedModule
import com.vp.detail.di.DetailActivityModule
import com.vp.favorites.di.FavoriteActivityModule
import com.vp.list.di.MovieListActivityModule
import com.vp.movies.MoviesApplication
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, NetworkModule::class, MovieListActivityModule::class, DetailActivityModule::class, DatabaseModule::class, FavoriteActivityModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: MoviesApplication): Builder

        @BindsInstance
        fun applicationContext(application: Context): Builder


        fun build(): AppComponent
    }

    fun inject(app: MoviesApplication)
}
