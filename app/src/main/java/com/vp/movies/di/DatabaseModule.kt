package com.vp.movies.di

import com.example.common.data.DatabaseRepository
import com.example.common.data.database.MoviesDatabaseManager
import com.example.common.data.file.FileManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabaseRepository(databaseManager: MoviesDatabaseManager, fileManager: FileManager) = DatabaseRepository(databaseManager, fileManager)
}