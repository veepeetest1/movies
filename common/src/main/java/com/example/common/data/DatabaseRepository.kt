package com.example.common.data

import com.example.common.data.database.MoviesDatabaseManager
import com.example.common.data.file.FileManager
import com.example.common.data.model.Movie
import javax.inject.Inject

class DatabaseRepository @Inject constructor(
        private val databaseManager: MoviesDatabaseManager,
        private val fileManager: FileManager
) {

    fun getAllMovies() = databaseManager.getAllMovies()

    fun getMovie(imdbId: String) = databaseManager.getMovie(imdbId)

    fun deleteMovie(imdbId: String) = databaseManager.deleteMovie(imdbId)

    fun insertMovie(movie: Movie) = databaseManager.insertMovie(movie)

    fun writeFile(imageUrl: String, imdbID: String) = fileManager.writeImage(imageUrl, imdbID)

    fun deleteFile(imdbID: String) = fileManager.deleteFile(imdbID)
}