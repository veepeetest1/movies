package com.example.common.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.common.data.database.MoviesDao
import com.example.common.data.model.Movie

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class MoviesDatabase : RoomDatabase(){
    abstract fun dao(): MoviesDao
}