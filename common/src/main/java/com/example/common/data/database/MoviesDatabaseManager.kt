package com.example.common.data.database

import android.content.Context
import androidx.room.Room
import com.example.common.data.model.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MoviesDatabaseManager @Inject constructor(
        context: Context
) {
    private val database: MoviesDatabase = Room.databaseBuilder(context, MoviesDatabase::class.java, "imdb").build()

    fun getAllMovies() = database.dao().getMovies()

    fun getMovie(imdbId: String) = database.dao().getMovie(imdbId)

    fun deleteMovie(imdbId: String) = flow { emit(database.dao().deleteMovie(imdbId)) }.flowOn(Dispatchers.Default)

    fun insertMovie(movie: Movie) = flow { emit(database.dao().insertMovie(movie)) }.flowOn(Dispatchers.Default)
}