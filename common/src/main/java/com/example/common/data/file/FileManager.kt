package com.example.common.data.file

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import javax.inject.Inject

class FileManager @Inject constructor(
        val context: Context
) {

    private fun createFile(name: String): File {
        val storage: File? = context.getExternalFilesDir(null)
        val appDirectory = File(storage, APP_FOLDER)
        if (!appDirectory.exists()) appDirectory.mkdir()

        val file = File(appDirectory, name)
        if (!file.exists()) file.createNewFile()

        return file
    }

    fun writeImage(urlString: String, name: String) = flow {
        try {
            val file = createFile(name)
            val url = URL(urlString)
            val inputStream = url.openStream()
            val outputStream = FileOutputStream(file)

            outputStream.write(inputStream.readBytes())
            outputStream.close()

            emit(file.absolutePath)
        } catch (exception: IOException) {
            emit("")
        }

    }.flowOn(Dispatchers.IO)


    fun deleteFile(imdbID: String) = flow {
        try {
            val storage: File? = context.getExternalFilesDir(null)
            val file = File(storage?.path + "/$APP_FOLDER/$imdbID")
            if (file.exists()) {
                file.delete()
            }
            emit(true)
        } catch (exception: IOException) {
            emit(false)
        }
    }

    companion object {
        const val APP_FOLDER = "favorite_movies_posters/"
    }
}