package com.example.common.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class Movie(
        @PrimaryKey
        val imdbID: String,
        val title: String,
        val runtime: String,
        val director: String,
        val plot: String,
        val poster: String,
        val localImageUrl: String?
)