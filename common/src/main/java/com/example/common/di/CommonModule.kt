package com.example.common.di

import android.content.Context
import com.example.common.data.DatabaseRepository
import com.example.common.data.database.MoviesDatabaseManager
import com.example.common.data.file.FileManager
import dagger.Module
import dagger.Provides

@Module
abstract class CommonComponent {

    @Provides
    fun provideFileManager(context: Context): FileManager = FileManager(context)

    @Provides
    fun provideDatabaseManager(context: Context): MoviesDatabaseManager = MoviesDatabaseManager(context)
}
