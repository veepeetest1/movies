package com.vp.detail

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.vp.detail.databinding.ActivityDetailBinding
import com.vp.detail.viewmodel.DetailsViewModel
import com.vp.detail.viewmodel.QueryResult
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class DetailActivity : DaggerAppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingActivityInjector

    @Inject
    lateinit var factory: DetailsViewModel.Factory
    private lateinit var detailViewModel: DetailsViewModel

    private var detailsMenu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        intent?.data?.getQueryParameter(BuildConfig.DETAILS_INTENT_URI_SUFFIX)?.let {
            detailViewModel = factory.create(it)
        } ?: throw IllegalStateException("You must provide movie id to display details")


        val binding: ActivityDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        binding.lifecycleOwner = this
        binding.viewModel = detailViewModel


        with(detailViewModel) {

            title().observe(this@DetailActivity, Observer { supportActionBar?.title = it })
            queryState().observe(this@DetailActivity, Observer(::onMovieRated))

            favoriteMovie().observe(this@DetailActivity, Observer { movie ->
                toggleStarMenuItem(true.takeIf { movie != null } ?: false)
            })
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val isFavorite = true.takeIf { detailViewModel.favoriteMovie().value != null } ?: false
        toggleStarMenuItem(isFavorite)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        detailsMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.star -> {
                detailViewModel.updateRating()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onMovieRated(state: QueryResult) {
        val message: String = when (state) {
            is QueryResult.Failed -> {
                getString(state.messageResourceID)
            }
            is QueryResult.Success -> {
                getString(state.messageResourceID)
            }
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun toggleStarMenuItem(isFavorite: Boolean) {
        val starItem = detailsMenu?.findItem(R.id.star) ?: return
        starItem.setIcon(R.drawable.ic_star_filled.takeIf { isFavorite } ?: R.drawable.ic_star)
    }

}
