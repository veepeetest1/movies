package com.vp.detail.di

import com.squareup.inject.assisted.dagger2.AssistedModule
import com.vp.detail.DetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DetailActivityModule {
    @ContributesAndroidInjector(modules = [DetailViewModelsModule::class, DetailNetworkModule::class, AssistedInjectModule::class])
    abstract fun bindDetailActivity(): DetailActivity
}


@AssistedModule
@Module(includes = [AssistedInject_AssistedInjectModule::class])
interface AssistedInjectModule