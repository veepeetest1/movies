package com.vp.detail.viewmodel

import androidx.lifecycle.*
import com.example.common.data.DatabaseRepository
import com.example.common.data.model.Movie
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.vp.detail.R
import com.vp.detail.model.MovieDetail
import com.vp.detail.service.DetailService
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback


@FlowPreview
class DetailsViewModel @AssistedInject constructor(
        private val detailService: DetailService,
        private val databaseRepository: DatabaseRepository,
        @Assisted val imdbID: String
) : ViewModel() {

    private val details: MutableLiveData<MovieDetail> = MutableLiveData()

    private val title: MutableLiveData<String> = MutableLiveData()
    private val loadingState: MutableLiveData<LoadingState> = MutableLiveData()
    private val queryResultState = MutableLiveData<QueryResult>()

    private val favoriteMovie: LiveData<Movie?> = databaseRepository
            .getMovie(imdbID)
            .catch { queryResultState.value = QueryResult.Failed(R.string.fetch_movie_error_message) }
            .asLiveData()


    fun title(): LiveData<String> = title

    fun details(): LiveData<MovieDetail> = details

    fun state(): LiveData<LoadingState> = loadingState

    fun queryState(): LiveData<QueryResult> = queryResultState

    fun favoriteMovie(): LiveData<Movie?> = favoriteMovie


    init {
        fetchDetails()
    }

    internal fun fetchDetails() {
        loadingState.value = LoadingState.IN_PROGRESS

        detailService.getMovie(imdbID).enqueue(object : Callback, retrofit2.Callback<MovieDetail> {
            override fun onResponse(call: Call<MovieDetail>?, response: Response<MovieDetail>?) {
                details.postValue(response?.body())

                response?.body()?.title?.let {
                    title.postValue(it)
                }

                loadingState.value = LoadingState.LOADED
            }

            override fun onFailure(call: Call<MovieDetail>?, t: Throwable?) {
                details.postValue(null)
                loadingState.value = LoadingState.ERROR
            }
        })
    }

    private fun delete(imdbID: String) {
        viewModelScope.launch {
            databaseRepository.deleteFile(imdbID)
                    .filter { it }
                    .flatMapConcat { databaseRepository.deleteMovie(imdbID) }
                    .catch { queryResultState.value = QueryResult.Failed() }
                    .onCompletion { queryResultState.value = QueryResult.Success() }
                    .collect()
        }
    }

    private fun insertMovie(selectedMovie: MovieDetail) {
        viewModelScope.launch {
            databaseRepository.writeFile(selectedMovie.poster, selectedMovie.imdbID)
                    .filter { it.isNotEmpty() }
                    .flatMapConcat {
                        val movie = selectedMovie.toMovie(withLocalImageUrl = it)
                        databaseRepository.insertMovie(movie)
                    }
                    .onCompletion { queryResultState.value = QueryResult.Success() }
                    .catch { queryResultState.value = QueryResult.Failed() }
                    .collect()
        }
    }

    fun updateRating() {
        val selectedMovie = this.details.value ?: return
        val isFavorite = this.favoriteMovie.value != null
        if (isFavorite) {
            delete(selectedMovie.imdbID)
        } else {
            insertMovie(selectedMovie)
        }
    }

    enum class LoadingState {
        IN_PROGRESS, LOADED, ERROR
    }

    private fun MovieDetail.toMovie(withLocalImageUrl: String) =
            Movie(
                    title = title,
                    runtime = runtime,
                    director = director,
                    imdbID = imdbID,
                    plot = plot,
                    poster = poster,
                    localImageUrl = withLocalImageUrl
            )

    @AssistedInject.Factory
    interface Factory {
        fun create(imdbID: String): DetailsViewModel
    }

}
