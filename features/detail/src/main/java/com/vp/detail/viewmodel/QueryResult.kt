package com.vp.detail.viewmodel

import com.vp.detail.R

sealed class QueryResult(
        messageResourceID: Int
) {
    data class Failed(val messageResourceID: Int = R.string.save_movie_error_message) : QueryResult(messageResourceID)
    data class Success(val messageResourceID: Int = R.string.save_movie_success_message) : QueryResult(messageResourceID)
}