package com.vp.detail.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.common.data.DatabaseRepository
import com.vp.detail.model.MovieDetail
import com.vp.detail.service.DetailService
import kotlinx.coroutines.FlowPreview
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.mock.Calls
import java.io.IOException

@FlowPreview
@RunWith(MockitoJUnitRunner::class)
class DetailsViewModelTest {

    private val imdbID = "tt2104994"

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var detailViewModel: DetailsViewModel

    private var databaseRepository: DatabaseRepository = mock(DatabaseRepository::class.java)
    private val detailService: DetailService = mock(DetailService::class.java)

    @Mock
    private lateinit var observer: Observer<MovieDetail?>

    @Mock
    private lateinit var loadingStateObserver: Observer<DetailsViewModel.LoadingState>

    @Captor
    private lateinit var loadingStateCaptor: ArgumentCaptor<DetailsViewModel.LoadingState>

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<MovieDetail>

    private lateinit var call: Call<MovieDetail>

    @Before
    fun setUp() {
        call = Calls.response(mock(MovieDetail::class.java))
        `when`(detailService.getMovie(anyString())).thenReturn(call)
        detailViewModel = DetailsViewModel(detailService, databaseRepository, imdbID)
        detailViewModel.details().observeForever(observer)
        detailViewModel.state().observeForever(loadingStateObserver)
    }

    @Test
    fun `should successfully fetch MovieDeTail`() {
        verify(observer, atLeastOnce()).onChanged(argumentCaptor.capture())
        val results = argumentCaptor.allValues
        Assert.assertTrue(results[0] != null)
        observer.onChanged(MovieDetail("", "", "", "", "", "", ""))

        verify(loadingStateObserver, atLeastOnce()).onChanged(loadingStateCaptor.capture())
        val loadingStateResults = loadingStateCaptor.allValues
        Assert.assertEquals(loadingStateResults[0], DetailsViewModel.LoadingState.LOADED)
    }

    @Test
    fun `should fail fetching MovieDetail`() {
        `when`(detailService.getMovie(anyString())).thenReturn(Calls.failure(IOException()))
        detailViewModel.fetchDetails()
        observer.onChanged(null)
    }

    @Test
    fun `should set loadingState to Error`() {
        `when`(detailService.getMovie(ArgumentMatchers.anyString())).thenAnswer {
            call.cancel()
            Calls.failure<IOException>(IOException())
        }

        detailViewModel.fetchDetails()

        loadingStateObserver.onChanged(DetailsViewModel.LoadingState.ERROR)
        Assert.assertEquals(detailViewModel.state().value, DetailsViewModel.LoadingState.ERROR)
    }

    @Test
    fun `should set MovieDetail fetching to LoadingState`() {
        `when`(detailService.getMovie(ArgumentMatchers.anyString())).thenAnswer { call.clone() }
        detailViewModel.fetchDetails()
        verify(loadingStateObserver, atLeastOnce()).onChanged(loadingStateCaptor.capture())
        val results = loadingStateCaptor.allValues
        Assert.assertEquals(results[1], DetailsViewModel.LoadingState.IN_PROGRESS) // precedent call @ setUp is at position 1
    }

    @After
    fun tearDown() {
        detailViewModel.details().removeObserver(observer)
        detailViewModel.state().removeObserver(loadingStateObserver)
    }
}