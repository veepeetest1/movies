package com.vp.favorites

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vp.favorites.ui.FavoriteAdapter
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_favorite.*
import javax.inject.Inject

class FavoriteActivity : DaggerAppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingActivityInjector

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var favoriteViewModel: FavoriteViewModel

    lateinit var adapter: FavoriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)
        favoriteViewModel = ViewModelProvider(this, factory).get(FavoriteViewModel::class.java)

        rv_favorites.layoutManager = LinearLayoutManager(this)
        adapter = FavoriteAdapter()
        rv_favorites.adapter = adapter

        favoriteViewModel.queryState().observe(this, Observer { state ->

            frame_ProgressBar.visibility = View.VISIBLE.takeIf { state is FavoriteViewModel.MoviesQueryState.Loading }
                    ?: View.GONE
            frame_defaultMessage.visibility = View.VISIBLE.takeIf { state is FavoriteViewModel.MoviesQueryState.Failed }
                    ?: View.GONE

            if (state !is FavoriteViewModel.MoviesQueryState.Success) return@Observer
            adapter.movies = state.movies

        })
    }
}