package com.vp.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.data.DatabaseRepository
import com.example.common.data.model.Movie
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoriteViewModel @Inject constructor(
        databaseRepository: DatabaseRepository
) : ViewModel() {

    private val queryState = MutableLiveData<MoviesQueryState>()

    fun queryState() = queryState

    init {
        viewModelScope.launch {
            databaseRepository.getAllMovies()
                    .onStart { queryState.value = MoviesQueryState.Loading }
                    .catch { queryState.value = MoviesQueryState.Failed }
                    .collect {
                        queryState.value = MoviesQueryState.Success(it)
                    }
        }
    }

    sealed class MoviesQueryState {
        object Failed : MoviesQueryState()
        object Loading : MoviesQueryState()
        data class Success(val movies: List<Movie>) : MoviesQueryState()
    }
}

