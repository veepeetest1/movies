package com.vp.favorites.ui

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.common.data.model.Movie
import com.vp.favorites.R
import kotlinx.android.synthetic.main.row_favorite_movie.view.*
import java.io.File

class FavoriteAdapter: RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {

    var movies: List<Movie>?= null
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder  {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_favorite_movie, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = movies?.count() ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        movies?.let {
            holder.bind(it[position])
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(movie: Movie) {
            with(itemView) {
                Glide.with(context).load(Uri.fromFile(File(movie.localImageUrl)))
                        .into(iv_Poster)

                tv_Title.text = movie.title
                tv_Plot.text = movie.plot
            }
        }
    }
}