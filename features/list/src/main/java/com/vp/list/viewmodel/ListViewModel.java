package com.vp.list.viewmodel;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.vp.list.model.ListItem;
import com.vp.list.model.SearchResponse;
import com.vp.list.service.SearchService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListViewModel extends ViewModel {

    static final String DEFAULT_SEARCH_QUERY = "Interview";
    static final int INITIAL_PAGE = 1;

    MutableLiveData<SearchResult> liveData = new MutableLiveData<>();
    private SearchService searchService;

    private String currentTitle = "";
    private List<ListItem> aggregatedItems = new ArrayList<>();

    private MutableLiveData<String> currentSearchQuery = new MutableLiveData<>();
    private MutableLiveData<Boolean> isListRefreshing = new MutableLiveData<>();

    @SuppressLint("CheckResult")
    @Inject
    ListViewModel(@NonNull SearchService searchService) {
        this.searchService = searchService;
        currentSearchQuery.setValue(DEFAULT_SEARCH_QUERY);
        searchMoviesByTitle(DEFAULT_SEARCH_QUERY, INITIAL_PAGE);
    }

    public LiveData<SearchResult> observeMovies() {
        return liveData;
    }

    public void searchMoviesByTitle(@NonNull String title, int page) {

        if (page == 1 & !title.equals(currentTitle)) {
            aggregatedItems.clear();
            currentTitle = title;
            liveData.setValue(SearchResult.inProgress());
        }

        searchService.search(title, page).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(@NonNull Call<SearchResponse> call, @NonNull Response<SearchResponse> response) {

                SearchResponse result = response.body();

                if (result != null) {
                    aggregatedItems.addAll(result.getSearch());
                    liveData.setValue(SearchResult.success(aggregatedItems, result.getTotalResults()));
                    isListRefreshing.setValue(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchResponse> call, @NonNull Throwable t) {
                liveData.setValue(SearchResult.error());
                isListRefreshing.setValue(false);
            }
        });
    }

    public MutableLiveData<Boolean> observeRefreshState() {
        return isListRefreshing;
    }

    public String getCurrentSearchQuery() {
        if (currentSearchQuery.getValue() == null) {
            return DEFAULT_SEARCH_QUERY;
        } else {
            return currentSearchQuery.getValue();
        }
    }

    public void setCurrentSearchQuery(String currentSearchQuery) {
        this.currentSearchQuery.setValue(currentSearchQuery);
    }

    public void refreshMoviesList() {
        isListRefreshing.setValue(true);
        searchMoviesByTitle(getCurrentSearchQuery(), INITIAL_PAGE);
    }
}
