package com.vp.list.viewmodel;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.vp.list.model.SearchResponse;
import com.vp.list.service.SearchService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.mock.Calls;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ListViewModelTest {


    @Rule
    public InstantTaskExecutorRule instantTaskRule = new InstantTaskExecutorRule();

    @Captor
    private ArgumentCaptor<SearchResult> captor;

    @Mock
    private Observer<SearchResult> observer;

    ListViewModel listViewModel;

    @Mock
    SearchService searchService;

    private Call<SearchResponse> call;

    @Before
    public void setUp() {
        call = Calls.response(mock(SearchResponse.class));
        when(searchService.search(anyString(), anyInt())).thenReturn(call);
        listViewModel = new ListViewModel(searchService);
        listViewModel.observeMovies().observeForever(observer);
    }

    @Test
    public void shouldReturnErrorState() {
        //given
        when(searchService.search(anyString(), anyInt())).thenReturn(Calls.failure(new IOException()));

        //when
        listViewModel.searchMoviesByTitle(ListViewModel.DEFAULT_SEARCH_QUERY, ListViewModel.INITIAL_PAGE);

        //then
        assertThat(listViewModel.observeMovies().getValue().getListState()).isEqualTo(ListState.ERROR);
    }

    @Test
    public void shouldReturnInProgressState() {
        //given
        when(searchService.search(anyString(), anyInt()))
                .thenAnswer(invocation -> call.clone());

        //when
        listViewModel.searchMoviesByTitle("", ListViewModel.INITIAL_PAGE); //empty string so that loading state's triggered
        verify(observer, atLeastOnce()).onChanged(captor.capture());
        List<SearchResult> results = captor.getAllValues();

        //then
        Assert.assertEquals(results.get(1), SearchResult.inProgress()); //first request state is at 0 index
        Assert.assertNotEquals(results.get(2), SearchResult.inProgress());
    }

    @Test
    public void shouldReturnLoadedInitialPage() {
        verify(observer, atLeastOnce()).onChanged(captor.capture());
        List<SearchResult> results = captor.getAllValues();

        Assert.assertEquals(results.get(0), SearchResult.success(new ArrayList<>(), 0));
    }

    @After
    public void tearDown() {
        listViewModel.observeMovies().removeObserver(observer);
    }
}